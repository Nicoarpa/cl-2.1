import Vue from 'vue'
import VueRouter from 'vue-router'
import {auth} from "@/firebase";


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/contact-library',
    name: 'Contact-library',
    component: () => import('../views/Contact-library.vue'),
      meta: {requiresAuth: true}

  },
    {
        path: '/Admin',
        name: 'Admin',
        component: () => import('../views/Admin.vue'),
        meta: {requiresAuth: true}

    }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth)){

        const usuario = auth.currentUser
        console.log('usuario desde router', usuario)

        if(!usuario){
            next({path: '/'
            })

    }else{
        next()
}
    }else{
    next()}
})



export default router
