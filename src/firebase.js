const firebase = require("firebase/app");
require("firebase/auth");
require("firebase/firestore");
require("firebase/storage");

const firebaseConfig = {
    apiKey: "AIzaSyCaSGRqu32W6POJc_jYMrkjLxQwg_Ma2d0",
    authDomain: "contactlibrary-2.firebaseapp.com",
    databaseURL: "https://contactlibrary-2.firebaseio.com",
    projectId: "contactlibrary-2",
    storageBucket: "contactlibrary-2.appspot.com",
    messagingSenderId: "334624459186",
    appId: "1:334624459186:web:3a40057f1b063af6937cab"
};

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const auth = firebase.auth();
const storage = firebase.storage();

export {firebase, db, auth, storage}
