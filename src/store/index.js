import Vue from 'vue'
import Vuex from 'vuex'
import {auth, db} from '@/firebase'
import router from '@/router'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        usuario:'',
        error:null
    },
    mutations: {
        nuevoUsuario(state, payload){
            if(payload === null){
                state.usuario = ''
            }else{
                state.usuario = payload
            }
            },
        setUsuario(state,payload){
            state.usuario = payload
        },
        setError(state, payload){
            state.error = payload
        },
        setContactos(state, payload){
            state.contacts = payload
        },

    },
    actions: {
    nuevoUsuario(state, payload){
        if(payload === null){
            state.usuario = ''
        }else{
            state.usuario = payload
        }
    }
,
        crearUsuario({commit},usuario) {
            auth.createUserWithEmailAndPassword(usuario.email, usuario.password)
                .then(res => {
                    console.log(res)
                    const usuario= {
                        email: res.user.email,
                        uid: res.user.uid,
                        foto: res.user.photoURL,
                    }

                    db.collection(res.user.email).add({
                        nombre: 'contacto numero 1'
                    }).then(doc => {
                        commit('setUsuario', usuarioCreado)
                        router.push('/Contact-library')
                    }).catch(error => console.log(error))
                }).catch(error => {
                    console.log(error)
                    commit('setError', error)
                })
},

        async setUsuario({commit}, user){

            try {
                const doc = await db.collection('usuarios').doc(user.uid).get()
                if(doc.exists){
                    commit('nuevoUsuario', doc.data())
                }else{
                    const usuario = {
                        nombre: user.displayName,
                        email: user.email,
                        uid: user.uid,
                        foto: user.photoURL
                    }
                    await db.collection('usuarios').doc(usuario.uid).set(
                        usuario
                    )
                    console.log('Usuario guardado en DB');
                    commit('nuevoUsuario', usuario)
                }

            } catch (error) {
                console.log(error);
            }


        },
        ingresoUsuario({commit},usuario){
            auth.signInWithEmailAndPassword(usuario.email,usuario.password)
                .then(res => {
                    console.log(res)
                    const usuarioLogeado= {
                        email:res.user.email,
                        uid:res.user.uid,
                        photo:res.user.photoURL
                    }
                    commit('setUsuario', usuarioLogeado)
                    router.push('/contact-library')
                })
                .catch(error => {
                    console.log(error)
                    commit('setError',error)
                })
        },
        cerrarSesion({commit}){
            auth.signOut()
                .then(() => {
                    router.push('/')
                })
        },
        detectarUsuario({commit},usuario){
            commit('setUsuario', usuario)
        },
        getContactslibrary({commit}){
            const contacts = []
            db.collection('contactslibrary').get()
                .then(res => {
                    res.forEach(doc =>{
                        console.log(doc.id)
                        console.log(doc.data())
                        let contacto = doc.data()
                        contacto.id = doc.id
                    })
                    commit('setContactos', contacts)
                })
        },
        agregarContacto({commit},nuevoContacto){
            db.collection('contactslibrary').add({
                name: nuevoContacto,
                number:nuevoContacto,
                email:nuevoContacto,
            })
                .then(doc => {
                    console.log(doc.id),
                    console.log(doc.data())
                })
        }


    },

    getters: {
        existeUsuario(state){
            if(state.usuario === null){
                return false
            }else{
                return true
            }
        }
    },
    modules: {
    },
})
